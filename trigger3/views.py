from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages

from login.views import namedtuplefetchall, dictfetchone, dictfetchall
from django.http import JsonResponse

import datetime

###########
# NOMOR 7 #
###########

def create_reservasi_rs(request):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('SELECT kode_faskes FROM RUMAH_SAKIT')
            list_rs = namedtuplefetchall(c)
            c.execute('SELECT nik FROM PASIEN')
            list_pasien = namedtuplefetchall(c)
            return render(request, 'reservasi_rs_create.html', {'list_rs': list_rs, 'list_pasien': list_pasien})

        elif request.method == 'POST':
            form = request.POST
            nik_pasien = form.get('nikpasien')
            tgl_masuk = form.get('tglmasuk')
            tgl_keluar = form.get('tglkeluar')
            kode_rs = form.get('koders')
            kode_ruangan = form.get('koderuangan')
            kode_bed = form.get('kodebed')

            # Mengurus tanggal
            tm_split = tgl_masuk.split('-')
            tgl_masuk = tm_split[2] + '-' + tm_split [1] + '-' + tm_split[0] + ' 00:00:00'
            tk_split = tgl_keluar.split('-')
            tgl_keluar = tk_split[2] + '-' + tk_split [1] + '-' + tk_split[0] + ' 23:59:59'

            c = connection.cursor()
            c.execute('''
                INSERT INTO RESERVASI_RS(kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed)
                VALUES({}, '{}', '{}', '{}', '{}', '{}')
            '''.format(nik_pasien, tgl_masuk, tgl_keluar, kode_rs, kode_ruangan, kode_bed))

            return redirect('/list_reservasi_rs')

    return redirect('/')
    
# Create reservasi rs ajax functions
def get_ruangan_of_rs(request):
    arg = request.GET['q']
    print(arg)
    c = connection.cursor()
    c.execute('''
    SELECT koderuangan FROM RUANGAN_RS WHERE koders='{}'
    '''.format(arg))
    
    data = namedtuplefetchall(c)

    return JsonResponse(data, safe=False)

def get_bed_of_rs(request):
    arg = request.GET['q']
    c = connection.cursor()
    c.execute('''
    SELECT kodebed FROM BED_RS WHERE koders='{}'
    '''.format(arg))
    
    data = namedtuplefetchall(c)

    return JsonResponse(data, safe=False)

def read_reservasi_rs(request):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('SELECT * FROM RESERVASI_RS')
        return render(request, 'reservasi_rs_read.html', {'reservasi_rs': namedtuplefetchall(c)})
        
    elif request.session['role'] == 'PENGGUNA_PUBLIK':
        username = request.session['username']
        print(username)
        c = connection.cursor()
        c.execute('''
            SELECT * FROM RESERVASI_RS WHERE kodepasien IN
            (SELECT nik FROM PASIEN WHERE idpendaftar = '{}')
        '''.format(username))
        return render(request, 'reservasi_rs_read.html', {'reservasi_rs': namedtuplefetchall(c)})
    
    return redirect('/')

def update_reservasi_rs(request, kodepasien, tglmasuk):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('''
                SELECT * FROM RESERVASI_RS
                WHERE kodepasien = '{}'
                AND tglmasuk = '{}'
            '''.format(kodepasien, tglmasuk))
            # print(namedtuplefetchall(c))
            return render(request, 'reservasi_rs_update.html', {'data':namedtuplefetchall(c)})
        
        elif request.method == 'POST':
            form = request.POST
            tgl_keluar = form.get('tglkeluar')

            # Mengurus tanggal
            tk_split = tgl_keluar.split('-')
            tgl_keluar = tk_split[2] + '-' + tk_split [1] + '-' + tk_split[0] + ' 23:59:59'

            c = connection.cursor()
            c.execute('''
                UPDATE RESERVASI_RS
                SET tglkeluar = '{}'
                WHERE kodepasien = '{}'
                AND tglmasuk = '{}'
            '''.format(tgl_keluar, kodepasien, tglmasuk))

            return redirect('/list_reservasi_rs')
    return redirect('/')

def delete_reservasi_rs(request, kodepasien, tglmasuk):
    if request.session['role'] == 'SATGAS':
        tm_split = tglmasuk.split()[0].split('-')
        tgl_masuk = datetime.datetime(int(tm_split[0]), int(tm_split[1]), int(tm_split[2]))

        if tgl_masuk > datetime.datetime.now():
            c = connection.cursor()
            c.execute('''
                DELETE FROM RESERVASI_RS
                WHERE kodepasien = '{}'
                AND tglmasuk = '{}'
            '''.format(kodepasien, tglmasuk))

            c.execute('SELECT * FROM RESERVASI_RS')
            data = {
                'reservasi_rs': namedtuplefetchall(c)
            }
            return redirect('/list_reservasi_rs')

        else:
            c = connection.cursor()
            c.execute('SELECT * FROM RESERVASI_RS')
            data = {
                'message': 'Reservasi tidak bisa dihapus',
                'reservasi_rs': namedtuplefetchall(c)
            }
            return render(request, 'reservasi_rs_read.html', data)
    
    return redirect('/')

###########
# NOMOR 8 #
###########

def create_faskes(request):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('''
                SELECT kode FROM FASKES ORDER BY kode DESC LIMIT 1
            ''')

            last_kode = str(dictfetchone(c).get('kode'))
            kode_split = []

            for i in range(len(last_kode)):
                kode_split.append(last_kode[i])
            
            kode_split.remove('F')

            last_num = int(''.join(kode_split))
            last_num += 1

            new_kode = 'F' + f"{last_num:02}"

            data = {
                'new_kode': new_kode
            }

            return render(request, 'faskes_create.html', data)

        elif request.method == 'POST':
            form = request.POST
            kode = form.get('kode')
            tipe = form.get('tipe')
            nama = form.get('nama')
            statusmilik = form.get('statusmilik')
            jalan = form.get('jalan')
            kelurahan = form.get('kelurahan')
            kecamatan = form.get('kecamatan')
            kabkot = form.get('kabkot')
            prov = form.get('prov')

            c = connection.cursor()
            c.execute('''
                INSERT INTO FASKES VALUES (
                    '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}'
                )
            '''.format(kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov))

            return redirect('/list_faskes')
    
    return redirect('/')

def read_faskes(request):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('SELECT * FROM FASKES')

        data = {
            'list_faskes': dictfetchall(c)
        }

        return render(request, 'faskes_read.html', data)
    
    return redirect('/')

def detail_faskes(request, kode):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('''
            SELECT * FROM FASKES
            WHERE kode = '{}'
        '''.format(kode))

        data = {
            'data': dictfetchone(c)
        }

        return render(request, 'faskes_detail.html', data)
    
    return redirect('/')

def update_faskes(request, kode):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('''
                SELECT * FROM FASKES
                WHERE kode = '{}'
            '''.format(kode))

            data = {
                'data': dictfetchone(c)
            }

            return render(request, 'faskes_update.html', data)

        elif request.method == 'POST':
            form = request.POST
            kode = form.get('kode')
            tipe = form.get('tipe')
            nama = form.get('nama')
            statusmilik = form.get('statusmilik')
            jalan = form.get('jalan')
            kelurahan = form.get('kelurahan')
            kecamatan = form.get('kecamatan')
            kabkot = form.get('kabkot')
            prov = form.get('prov')

            c = connection.cursor()
            c.execute('''
                UPDATE FASKES
                SET (tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov) =
                ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
                WHERE kode = '{}'
            '''.format(tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov, kode))

            return redirect('/list_faskes')

    return redirect('/')

def delete_faskes(request, kode):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('''
            DELETE FROM FASKES
            WHERE kode='{}'
        '''.format(kode))
        return redirect('/list_faskes')
    
    return redirect('/')

###########
# NOMOR 9 #
###########

def create_jadwal_faskes(request):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('SELECT kode FROM FASKES')

            data = {
                'list_kode': dictfetchall(c)
            }

            return render(request, 'jadwal_faskes_create.html', data)

        elif request.method == 'POST':
            form = request.POST
            kodefaskes = form.get('kodefaskes')
            shift = form.get('shift')
            tanggal = form.get('tanggal')

            # Mengurus tanggal
            tgl_split = tanggal.split('-')
            tanggal = tgl_split[2] + '-' + tgl_split [1] + '-' + tgl_split[0]

            c = connection.cursor()
            c.execute('''
                INSERT INTO JADWAL VALUES (
                    '{}', '{}', '{}'
                )
            '''.format(kodefaskes, shift, tanggal))

            return redirect('/list_jadwal_faskes')

    return redirect('/')

def read_jadwal_faskes(request):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('SELECT * FROM JADWAL')
        
        data = {
            'list_jadwal': dictfetchall(c)
        }

        return render(request, 'jadwal_faskes_read.html', data)
    
    return redirect('/')

############
# NOMOR 10 #
############

def create_rs(request):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('SELECT kode FROM FASKES')

            data = {
                'list_kode': dictfetchall(c)
            }

            return render(request, 'rs_create.html', data)

        elif request.method == 'POST':
            form = request.POST
            kodefaskes = form.get('kodefaskes')
            isrujukan = form.get('isrujukan')

            if isrujukan == None:
                isrujukan = '0'

            try:
                c = connection.cursor()
                c.execute('''
                    INSERT INTO RUMAH_SAKIT VALUES (
                        '{}', '{}'
                    )
                '''.format(kodefaskes, isrujukan))

                return redirect('/list_rs')
            except:
                c.execute('SELECT kode FROM FASKES')

                data = {
                    'list_kode': dictfetchall(c),
                    'message': "Faskes yang anda pilih sudah berstatus Rumah Sakit"
                }

                return render(request, 'rs_create.html', data)
    
    return redirect('/')
            

def read_rs(request):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('SELECT * FROM RUMAH_SAKIT')

        data = {
            'list_rs': dictfetchall(c)
        }

        return render(request, 'rs_read.html', data)
    
    return redirect('/')

def update_rs(request, kodefaskes):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('''
                SELECT * FROM RUMAH_SAKIT
                WHERE kode_faskes = '{}'
            '''.format(kodefaskes))

            data = {
                'data': dictfetchone(c)
            }

            return render(request, 'rs_update.html', data)
        
        elif request.method == 'POST':
            form = request.POST
            kodefaskes = form.get('kodefaskes')
            isrujukan = form.get('isrujukan')

            if isrujukan == None:
                isrujukan = '0'

            c = connection.cursor()
            c.execute('''
                UPDATE RUMAH_SAKIT
                SET isrujukan = '{}'
                WHERE kode_faskes = '{}'
            '''.format(isrujukan, kodefaskes))

            return redirect('/list_rs')

    return redirect('/')

############
# NOMOR 11 #
############

def read_transaksi_rs(request):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('SELECT * FROM TRANSAKSI_RS')

        data = {
            'list_transaksi': dictfetchall(c)
        }

        return render(request, 'transaksi_rs_read.html', data)

    return redirect('/')

def update_transaksi_rs(request, idtransaksi):
    if request.session['role'] == 'SATGAS':
        if request.method == 'GET':
            c = connection.cursor()
            c.execute('''
                SELECT * FROM TRANSAKSI_RS
                WHERE idtransaksi = '{}'
            '''.format(idtransaksi))

            data = {
                'data': dictfetchone(c)
            }

            return render(request, 'transaksi_rs_update.html', data)
        
        elif request.method == 'POST':
            form = request.POST
            idtransaksi = form.get('idtransaksi')
            statusbayar = form.get('statusbayar')

            c = connection.cursor()
            c.execute('''
                UPDATE TRANSAKSI_RS
                SET statusbayar = '{}'
                WHERE idtransaksi = '{}'
            '''.format(statusbayar, idtransaksi))

            return redirect('/list_transaksi_rs')
    
    return redirect('/')

def delete_transaksi_rs(request, idtransaksi):
    if request.session['role'] == 'SATGAS':
        c = connection.cursor()
        c.execute('''
            DELETE FROM TRANSAKSI_RS
            WHERE idtransaksi = '{}'
        '''.format(idtransaksi))

        return redirect('/list_transaksi_rs')

    return redirect('/')
