from django.urls import path

from . import views

app_name = 'trigger3'

urlpatterns = [
    path('list_reservasi_rs/', views.read_reservasi_rs, name='list_reservasi_rs'),
    path('create_reservasi_rs/', views.create_reservasi_rs, name='create_reservasi_rs'),
    path('get_ruangan_of_rs/', views.get_ruangan_of_rs, name='get_ruangan_of_rs'),
    path('get_bed_of_rs', views.get_bed_of_rs, name='get_bed_of_rs'),
    path('delete_reservasi_rs/<int:kodepasien>/<slug:tglmasuk>', views.delete_reservasi_rs, name='delete_reservasi_rs'),
    path('update_reservasi_rs/<int:kodepasien>/<slug:tglmasuk>', views.update_reservasi_rs, name='update_reservasi_rs'),
    path('list_faskes/', views.read_faskes, name='read_faskes'),
    path('detail_faskes/<slug:kode>/', views.detail_faskes, name='detail_faskes'),
    path('create_faskes/', views.create_faskes, name='create_faskes'),
    path('delete_faskes/<slug:kode>/', views.delete_faskes, name='delete_faskes'),
    path('update_faskes/<slug:kode>/', views.update_faskes, name='update_faskes'),
    path('create_jadwal_faskes/', views.create_jadwal_faskes, name='create_jadwal_faskes'),
    path('list_jadwal_faskes/', views.read_jadwal_faskes, name='read_jadwal_faskes'),
    path('create_rs/', views.create_rs, name='create_rs'),
    path('list_rs/', views.read_rs, name='read_rs'),
    path('update_rs/<slug:kodefaskes>/', views.update_rs, name='update_rs'),
    path('list_transaksi_rs/', views.read_transaksi_rs, name='read_transaksi_rs'),
    path('update_transaksi_rs/<slug:idtransaksi>', views.update_transaksi_rs, name='update_transaksi_rs'),
    path('delete_transaksi_rs/<slug:idtransaksi>', views.delete_transaksi_rs, name='delete_transaksi_rs')
]