from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages

from login.views import namedtuplefetchall, dictfetchone

def index(request):
    try:
        if request.session['role'] == 'ADMIN_SISTEM':
            return dashboardAdmin(request)
        elif request.session['role'] == 'PENGGUNA_PUBLIK':
            return dashboardPublik(request)
        elif request.session['role'] == 'SATGAS':
            return dashboardSatgas(request)
        elif request.session['role'] == 'DOKTER':
            return dashboardDokter(request)
    except:
        return render(request, 'index.html')

def dashboardAdmin(request):
    c = connection.cursor()
    c.execute('SELECT * FROM ADMIN WHERE username=%s', [request.session['username']])
    return render(request, 'dashboard_admin.html', {'data': dictfetchone(c)})

def dashboardPublik(request):
    c = connection.cursor()
    c.execute('SELECT * FROM PENGGUNA_PUBLIK WHERE username=%s', [request.session['username']])
    return render(request, 'dashboard_publik.html', {'data': dictfetchone(c)})

def dashboardSatgas(request):
    c = connection.cursor()
    c.execute('SELECT * FROM ADMIN_SATGAS WHERE username=%s', [request.session['username']])
    return render(request, 'dashboard_satgas.html', {'data': dictfetchone(c)})

def dashboardDokter(request):
    c = connection.cursor()
    c.execute('SELECT * FROM DOKTER WHERE username=%s', [request.session['username']])
    return render(request, 'dashboard_dokter.html', {'data': dictfetchone(c)})