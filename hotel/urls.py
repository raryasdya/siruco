from django.urls import path

from . import views

app_name = 'hotel'

urlpatterns = [
    path('hotel/', views.read_hotel, name='readhotel'),
    path('update/', views.update_hotel, name='updatehotel'),
    path('create/', views.create_hotel, name='createhotel'),
    path('delete/', views.delete_hotel, name='deletehotel'),
    path('rsv/', views.read_reservasi_hotel, name='readrsvhotel'),
    path('rsvupdate/', views.update_reservasi_hotel, name='updatersvhotel'),
    path('rsvcreate/', views.create_reservasi_hotel, name='creatersvhotel'),
    path('rsvdelete/', views.delete_reservasi_hotel, name='deletersvhotel'),
    path('trs/', views.read_transaksi_hotel, name='readtrshotel'),
    path('trsupdate/', views.update_transaksi_hotel, name='updatetrshotel'),
    # path('trscreate/', views.create_transaksi_hotel, name='createtrshotel'),
    path('trsdelete/', views.delete_transaksi_hotel, name='deletetrshotel'),
    path('bkg/', views.read_transaksi_booking, name='readtrsbooking'),
    # path('bkgcreate/', views.create_transaksi_booking, name='createtrsbooking'),
    path('bkgdelete/', views.delete_transaksi_booking, name='deletetrsbooking'),

]