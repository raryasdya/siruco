from django.db import connection
from django.shortcuts import redirect, render

# Create your views here.

# def create_hotel(request):
#     if request.session["user_role"] == "Admin Sistem":
#         try:
#             KodeHotel = request.POST["KodeHotel"]

def create_hotel(request):
    if request.session["role"] == "ADMIN_SISTEM":
        context = {}
        if request.method == 'POST':
            try:
                KodeHotel = request.POST["KodeHotel"]
                JenisBed = request.POST["JenisBed"]
                Tipe = request.POST["Tipe"]
                Harga = request.POST["Harga"]
                # mencari max no_urut
                KodeRoom = "RM"
                no = 1
                with connection.cursor() as c:
                    # mengambil nomor urut terbesar terakhir
                    c.execute(
                        f"select KodeRoom from hotel_room where KodeHotel = '{KodeHotel}'")
                    row = c.fetchall()
                    rownew = str(row[-1])
                    x = int(rownew[4:7])
                    # jika table pelanggan sudah ada isinya
                    if rownew != None:
                        # no = str(int(row[0]) + 1)
                        no = x + 1
                sno = str(no)
                zno = sno
                if len(sno) < 3:
                    zno = sno.zfill(3)
                KodeRoom += zno

                # insert data
                with connection.cursor() as c:
                    command = f"INSERT INTO hotel_room VALUES"
                    command += f"('{KodeHotel}', '{KodeRoom}', '{JenisBed}', '{Tipe}', '{Harga}')"
                    # command = "INSERT INTO hotel_room VALUES "
                    # command += str(KodeHotel, KodeRoom, JenisBed,
                    #                 Tipe, str(Harga))
                    c.execute(command)
            except Exception as e:
                print(e)
                context["error"] = "Data yang Anda masukkan tidak valid mohon cek kembali"
                # mengambil semua kode
                # with connection.cursor() as c:
                #     c.execute("select * from item")
                #     context["daftar_kode"] = c.fetchall()
                # # mengambil semua email
                # with connection.cursor() as c:
                #     c.execute("select email from pelanggan")
                #     context["daftar_email"] = c.fetchall()
                return render(request, 'hotel/createhotel.html', context)
            return redirect('hotel:readhotel')
        else:
            data = []
            # mengambil semua kode
            with connection.cursor() as c:
                c.execute("select * from Hotel")
                context["daftar_Hotel"] = c.fetchall()
            # mengambil semua email
            with connection.cursor() as c:
                c.execute("select Kode from Hotel")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data.append((x[0]))
                context["data"] = data
            return render(request, 'hotel/createhotel.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def read_hotel(request):
    if request.session["role"] in ["SATGAS", "PENGGUNA_PUBLIK", "ADMIN_SISTEM"]:
        data = []
        context = {}
        # mengambil semua daftar laundry
        with connection.cursor() as c:
            # if request.session["user_role"] == "pelanggan":
            #     email = request.session["user_email"]
            #     c.execute(f"select * from daftar_laundry where email = '{email}'")
            # else:
            #     c.execute("select * from daftar_laundry")
            c.execute("select * from hotel_room")
            dataSQL = c.fetchall()

            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], x[1], x[2], x[3], str(x[4])))
            context["data"] = data
        return render(request, 'hotel/readhotel.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def delete_hotel(request):
    if request.session["role"] == "ADMIN_SISTEM":
        if request.method == "POST":
            with connection.cursor() as c:
                KodeHotel = request.POST["KodeHotel"]
                KodeRoom = request.POST["KodeRoom"]
                command = f"delete from hotel_room where KodeHotel = '{KodeHotel}' and "
                command += f"KodeRoom = '{KodeRoom}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('hotel:readhotel')
    else:
        return render(request, 'hotel/gagal.html')

def update_hotel(request):
    if request.session["role"] == "ADMIN_SISTEM":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["KodeHotel"] = request.GET["KodeHotel"]
                context["KodeRoom"] = request.GET["KodeRoom"]
                context["JenisBed"] = request.GET["JenisBed"]
                context["Tipe"] = request.GET["Tipe"]
                context["Harga"] = request.GET["Harga"]
            # jika tidak ada
            except:
                return redirect('hotel:readhotel')
            # mengambil semua kode item
            # with connection.cursor() as c:
            #     c.execute("select * from item")
            #     context["daftar_kode"] = c.fetchall()
            return render(request, 'hotel/updatehotel.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                KodeHotel = request.POST["KodeHotel"]
                KodeRoom = request.POST["KodeRoom"]
                JenisBed = request.POST["JenisBed"]
                Tipe = request.POST["Tipe"]
                Harga = request.POST["Harga"]
                command = f"update hotel_room set "
                command += f"JenisBed = '{JenisBed}', Tipe = '{Tipe}', Harga = '{Harga}' "
                command += f"where KodeHotel = '{KodeHotel}' and KodeRoom = '{KodeRoom}' "
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('hotel:readhotel')
    else:
        return render(request, 'hotel/gagal.html')

def create_reservasi_hotel(request):
    if request.session["role"] in ["SATGAS", "PENGGUNA_PUBLIK"]:
        context = {}
        if request.method == 'POST':
            try:
                KodePasien = request.POST["KodePasien"]
                TglMasuk = request.POST["TglMasuk"]
                TglKeluar = request.POST["TglKeluar"]
                KodeHotel = request.POST["KodeHotel"]
                KodeRoom = request.POST["KodeRoom"]

                # insert data
                with connection.cursor() as c:
                    command = f"INSERT INTO reservasi_hotel VALUES"
                    command += f"('{KodePasien}', '{TglMasuk}', '{TglKeluar}', '{KodeHotel}', '{KodeRoom}')"
                    c.execute(command)
            except Exception as e:
                print(e)
                context["error"] = "Data yang Anda masukkan tidak valid mohon cek kembali"
                # mengambil semua kode
                # with connection.cursor() as c:
                #     c.execute("select * from item")
                #     context["daftar_kode"] = c.fetchall()
                # # mengambil semua email
                # with connection.cursor() as c:
                #     c.execute("select email from pelanggan")
                #     context["daftar_email"] = c.fetchall()
                return render(request, 'hotel/creatersvhotel.html', context)
            return redirect('hotel:readrsvhotel')
        else:
            data_nik = []
            data_hotel = []
            data_room = []
            # mengambil semua email
            with connection.cursor() as c:
                c.execute("select NIK from PASIEN")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data_nik.append((x[0]))
                context["data_nik"] = data_nik

            with connection.cursor() as c:
                c.execute("select Kode from HOTEL")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data_hotel.append((x[0]))
                context["data_hotel"] = data_hotel

            with connection.cursor() as c:
                c.execute("select KodeRoom from HOTEL_ROOM")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data_room.append((x[0]))
                context["data_room"] = data_room
            
            return render(request, 'hotel/creatersvhotel.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def read_reservasi_hotel(request):
    if request.session["role"] in ["SATGAS", "PENGGUNA_PUBLIK"]:
        data = []
        context = {}
        # mengambil semua daftar laundry
        with connection.cursor() as c:
            # if request.session["user_role"] == "pelanggan":
            #     email = request.session["user_email"]
            #     c.execute(f"select * from daftar_laundry where email = '{email}'")
            # else:
            #     c.execute("select * from daftar_laundry")
            c.execute("select * from reservasi_hotel")
            dataSQL = c.fetchall()

            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], str(x[1]), str(x[2]), x[3], x[4]))
            context["data"] = data
        return render(request, 'hotel/readrsvhotel.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def delete_reservasi_hotel(request):
    if request.session["role"] == "SATGAS":
        if request.method == "POST":
            with connection.cursor() as c:
                KodePasien = request.POST["KodePasien"]
                TglMasuk = request.POST["TglMasuk"]
                command = f"delete from reservasi_hotel where KodePasien = '{KodePasien}' and "
                command += f"TglMasuk = '{TglMasuk}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('hotel:readrsvhotel')
    else:
        return render(request, 'hotel/gagal.html')

def update_reservasi_hotel(request):
    if request.session["role"] == "SATGAS":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["KodePasien"] = request.GET["KodePasien"]
                context["TglMasuk"] = request.GET["TglMasuk"]
                context["TglKeluar"] = request.GET["TglKeluar"]
                context["KodeHotel"] = request.GET["KodeHotel"]
                context["KodeRoom"] = request.GET["KodeRoom"]
            # jika tidak ada
            except:
                return redirect('hotel:readrsvhotel')
            # mengambil semua kode item
            # with connection.cursor() as c:
            #     c.execute("select * from item")
            #     context["daftar_kode"] = c.fetchall()
            return render(request, 'hotel/updatersvhotel.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                KodePasien = request.POST["KodePasien"]
                TglMasuk = request.POST["TglMasuk"]
                TglKeluar = request.POST["TglKeluar"]
                KodeHotel = request.POST["KodeHotel"]
                KodeRoom = request.POST["KodeRoom"]
                command = f"update reservasi_hotel set "
                command += f"TglKeluar = '{TglKeluar}'"
                command += f"where KodePasien = '{KodePasien}' and TglMasuk = '{TglMasuk}' "
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('hotel:readrsvhotel')
    else:
        return render(request, 'hotel/gagal.html')

def read_transaksi_hotel(request):
    if request.session["role"] in ["SATGAS", "PENGGUNA_PUBLIK"]:
        data = []
        context = {}
        # mengambil semua daftar laundry
        with connection.cursor() as c:
            # if request.session["user_role"] == "pelanggan":
            #     email = request.session["user_email"]
            #     c.execute(f"select * from daftar_laundry where email = '{email}'")
            # else:
            #     c.execute("select * from daftar_laundry")
            c.execute("select * from transaksi_hotel")
            dataSQL = c.fetchall()

            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], x[1], str(x[2]), str(x[3]), str(x[4]), x[5]))
            context["data"] = data
        return render(request, 'hotel/readtrshotel.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def delete_transaksi_hotel(request):
    if request.session["role"] == "SATGAS":
        if request.method == "POST":
            with connection.cursor() as c:
                IdTransaksi = request.POST["IdTransaksi"]
                KodePasien = request.POST["KodePasien"]
                command = f"delete from transaksi_hotel where IdTransaksi = '{IdTransaksi}' and "
                command += f"KodePasien = '{KodePasien}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('hotel:readtrshotel')
    else:
        return render(request, 'hotel/gagal.html')

def update_transaksi_hotel(request):
    if request.session["role"] == "SATGAS":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["IdTransaksi"] = request.GET["IdTransaksi"]
                context["KodePasien"] = request.GET["KodePasien"]
                context["TanggalPembayaran"] = request.GET["TanggalPembayaran"]
                context["WaktuPembayaran"] = request.GET["WaktuPembayaran"]
                context["TotalBayar"] = request.GET["TotalBayar"]
                context["StatusBayar"] = request.GET["StatusBayar"]
            # jika tidak ada
            except:
                return redirect('hotel:readtrshotel')
            # mengambil semua kode item
            # with connection.cursor() as c:
            #     c.execute("select * from item")
            #     context["daftar_kode"] = c.fetchall()
            return render(request, 'hotel/updatetrshotel.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                IdTransaksi = request.POST["IdTransaksi"]
                KodePasien = request.POST["KodePasien"]
                TanggalPembayaran = request.POST["TanggalPembayaran"]
                WaktuPembayaran = request.POST["WaktuPembayaran"]
                TotalBayar = request.POST["TotalBayar"]
                StatusBayar = request.POST["StatusBayar"]
                command = f"update transaksi_hotel set "
                command += f"StatusBayar = '{StatusBayar}'"
                command += f"where IdTransaksi = '{IdTransaksi}'"
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('hotel:readtrshotel')
    else:
        return render(request, 'hotel/gagal.html')

def read_transaksi_booking(request):
    if request.session["role"] in ["SATGAS", "PENGGUNA_PUBLIK"]:
        data = []
        context = {}
        # mengambil semua daftar laundry
        with connection.cursor() as c:
            # if request.session["user_role"] == "pelanggan":
            #     email = request.session["user_email"]
            #     c.execute(f"select * from daftar_laundry where email = '{email}'")
            # else:
            #     c.execute("select * from daftar_laundry")
            c.execute("select * from transaksi_booking")
            dataSQL = c.fetchall()

            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], str(x[1]), x[2], str(x[3])))
            context["data"] = data
        return render(request, 'hotel/readtrsbooking.html', context)
    else:
        return render(request, 'hotel/gagal.html')

def delete_transaksi_booking(request):
    if request.session["role"] == "SATGAS":
        if request.method == "POST":
            with connection.cursor() as c:
                IdTransaksiBooking = request.POST["IdTransaksiBooking"]
                command = f"delete from transaksi_booking where IdTransaksiBooking = '{IdTransaksiBooking}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('hotel:readtrsbooking')
    else:
        return render(request, 'hotel/gagal.html')