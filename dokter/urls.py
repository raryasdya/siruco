from django.urls import path
from . import views

app_name = 'dokter'

urlpatterns = [
    path('createJadwalDokter/', views.jadwalDokter, name = 'createJadwalDokter'),
    path('jadwalDokter/',views.jadwalDokterList,name = 'jadwalDokter' ),
    path('createAppointment/',views.createAppointment,name = 'createAppointment'),
    path('appointment/',views.appointment,name = 'Appointment'),
    path('createRuanganRS/',views.createRuanganRS,name = 'createRuanganRS'),
    path('RuanganRS/',views.RuanganRS,name = 'RuanganRS'),
    path('createBed/',views.createBed,name = 'createBed'),
    path('bed/',views.bed,name = 'bed'),
]