import datetime
import json

from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages


# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def jadwalDokter(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'DOKTER'):
        if (request.method == 'POST'):
            return createJadwalDokter(request)
        else:
            return showCreateJadwalDokter(request)
    else:
        # w
        return redirect('/')


def showCreateJadwalDokter(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    c.execute('''
    SELECT j.kode_faskes, j.shift, j.tanggal FROM jadwal AS j
    LEFT JOIN jadwal_dokter AS jd
    ON j.kode_faskes = jd.kode_faskes AND j.shift = jd.shift AND j.tanggal = jd.tanggal
     WHERE jd.username is null
    ''')
    username = request.session.get('username')
    # print(username)
    jadwal = namedtuplefetchall(c)
    context = {
        'jadwal_list': jadwal,
    }
    return render(request, 'createJadwalDokter.html', context)


def createJadwalDokter(request):
    global kodeFaskes, shift, tanggal
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    kodeFaskes = data['kodeFaskes']
    shift = data['shift']
    tanggal = data['tanggal']

    c.execute('''
    SELECT nostr FROM Dokter
    WHERE username = '{}'
    '''.format(username))
    nostr = namedtuplefetchall(c)
    nostr2 = nostr[0].nostr
    try:
        c.execute('''
        INSERT INTO jadwal_dokter(nostr,username,kode_faskes,shift,tanggal,jmlpasien)
        VALUES ('{}', '{}', '{}', '{}', '{}', '{}')
        '''.format(nostr2, username, kodeFaskes, shift, tanggal, 0))
    except:
        pass

    return redirect('/RumahSakit/jadwalDokter')


def jadwalDokterList(request):
    global jadwal
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    username = request.session.get('username')
    # print(role)
    # print(username)
    if (username == None):
        return redirect('/login')
    else:
        if (role == 'SATGAS' or role == 'PENGGUNA_PUBLIK' or role == 'DOKTER'):
            if (role == "SATGAS" or role == 'PENGGUNA_PUBLIK'):
                c.execute('''
                SELECT * FROM jadwal_dokter
                ''')
                jadwal = namedtuplefetchall(c)

            else:
                c.execute('''
                SELECT * FROM jadwal_dokter
                WHERE username = '{}'
                '''.format(username))
                jadwal = namedtuplefetchall(c)
        else:
            return redirect('/')
    context = {
        'jadwal_dokter': jadwal
    }
    return render(request, 'jadwalDokter.html', context)


def createAppointment(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS' or role == 'PENGGUNA_PUBLIK'):
        if (request.method == 'POST'):
            return createAppointmentUser(request)
        else:
            return showCreateAppointment(request)
    else:
        return redirect('/')


def showCreateAppointment(request, validasi=None):
    global nik, message
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    username = request.session.get('username')
    message = ''
    c.execute('''
             SELECT * FROM jadwal_dokter
             ''')
    jadwal = namedtuplefetchall(c)
    if (role == 'PENGGUNA_PUBLIK'):
        c.execute('''
                SELECT nik FROM Pasien
                WHERE idpendaftar = '{}'
                '''.format(username))
        nik = namedtuplefetchall(c)
    else:
        c.execute('''
                   SELECT nik FROM Pasien
                    ''')
        nik = namedtuplefetchall(c)
    if (validasi == False):
        message = '“Silahkan pilih shift dan tanggal lainnya,karena shift dan tanggal yang dipilih sudah penuh'
    context = {
        'jadwal_dokter': jadwal,
        'nikList': nik,
        'message': message,
    }

    return render(request, 'createAppointment.html', context)


def createAppointmentUser(request):
    global kodeFaskes, shift, tanggal
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    nik = data['nik']
    email = data['email']
    kodeFaskes = data['kodeFaskes']
    shift = data['shift']
    tanggal = data['tanggal']

    c.execute('''
        SELECT nostr FROM Dokter
        WHERE username = '{}'
        '''.format(email))
    nostr = namedtuplefetchall(c)
    nostr2 = nostr[0].nostr
    try:
        c.execute('''
            INSERT INTO Memeriksa(nik_pasien,nostr,username_dokter,kode_faskes,praktek_shift,praktek_tgl)
            VALUES ('{}', '{}', '{}', '{}', '{}', '{}')
            '''.format(nik, nostr2, email, kodeFaskes, shift, tanggal))
    except Exception as e:
        print(e)
        return showCreateAppointment(request, False)
    return redirect('/RumahSakit/appointment')


def appointment(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS' or role == 'PENGGUNA_PUBLIK' or role == 'DOKTER'):
        if (request.method == 'POST'):
            if (role == 'SATGAS'):
                return deleteAppointment(request)
            elif (role == 'DOKTER'):
                return updateAppointment(request)
        else:
            return showAppointment(request)
    else:
        return redirect('/')


def showAppointment(request):
    global appointment
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    print(role)
    username = request.session.get('username')
    if (role == 'PENGGUNA_PUBLIK'):
        c.execute('''
                   SELECT m.nik_pasien, m.username_dokter, m.kode_faskes, m.praktek_shift, m.praktek_tgl, m.rekomendasi
                    FROM Memeriksa AS m 
                    INNER JOIN Pasien as p
                    ON p.idpendaftar = '{}'
                   '''.format(username))
        appointment = namedtuplefetchall(c)
    elif (role == 'DOKTER'):
        c.execute('''
        SELECT * FROM Memeriksa
        WHERE username_dokter = '{}'
        '''.format(username))
        appointment = namedtuplefetchall(c)
    else:
        c.execute('''
                      SELECT * FROM Memeriksa
                       ''')
        appointment = namedtuplefetchall(c)
    # print(appointment)
    context = {
        'role': role,
        'appointment': appointment,
    }

    return render(request, 'appointment.html', context)


def deleteAppointment(request):
    global kodeFaskes, shift, tanggal
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    nik = data['nik']
    str = data['str']
    email = data['email']
    kodeFaskes = data['kodeFaskes']
    shift = data['shift']
    tanggal = data['tanggal']
    c.execute('''
              UPDATE Jadwal_dokter
              set jmlpasien = jmlpasien -1
              WHERE nostr = '{}'
              AND username = '{}'
              AND kode_faskes = '{}'
              AND shift = '{}'
              AND tanggal = '{}'   
              '''.format(str, email, kodeFaskes, shift, tanggal))
    c.execute('''
                  DELETE FROM Memeriksa
                  WHERE nik_pasien = '{}' 
                  AND nostr = '{}'
                  AND username_dokter = '{}'
                  AND kode_faskes = '{}'
                  AND praktek_shift = '{}'
                  AND praktek_tgl = '{}'   
                  '''.format(nik, str, email, kodeFaskes, shift, tanggal))
    return redirect('dokter:Appointment')


def updateAppointment(request):
    global kodeFaskes, shift, tanggal
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    nik = data['nik']
    str = data['str']
    email = data['email']
    kodeFaskes = data['kodeFaskes']
    shift = data['shift']
    tanggal = data['tanggal']
    rekomendasi = data['rekomendasi']
    c.execute('''
                      UPDATE Memeriksa
                      set rekomendasi = '{}'
                      WHERE nik_pasien = '{}' 
                      AND nostr = '{}'
                      AND username_dokter = '{}'
                      AND kode_faskes = '{}'
                      AND praktek_shift = '{}'
                      AND praktek_tgl = '{}'   
                      '''.format(rekomendasi, nik, str, email, kodeFaskes, shift, tanggal))
    return redirect('dokter:Appointment')


def createRuanganRS(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS'):
        if (request.method == 'POST'):
            return createRuanganRSUser(request)
        else:
            return showCreateRuanganRS(request)
    else:
        # w
        return redirect('/')



def showCreateRuanganRS(request):
    global rumahSakit, ruanganRS
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    username = request.session.get('username')
    c.execute('''SELECT kode_faskes FROM Rumah_sakit ''')
    rumahSakit = namedtuplefetchall(c)
    context = {
    'rsList': rumahSakit,
    }
    return render(request, 'createRuanganRS.html', context)

def createRuanganRSUser(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    rs = data['koders']
    tipe = data['tipe']
    harga = data['harga']
    c.execute('''
           SELECT SUBSTRING (koderuangan, 3)AS kode_ruangan, SUBSTRING(koderuangan,1,3) AS str_ruangan   FROM ruangan_rs where koders = '{}'
           order by koderuangan desc
           '''.format(rs))
    res = namedtuplefetchall(c)
    if res:
        kode_ruangan = res[0].kode_ruangan
        kode_ruangan = int(kode_ruangan)+1
    else:
        kode_ruangan = 0
    str_ruangan = res[0].str_ruangan
    str_kode_ruangan = str_ruangan + str(kode_ruangan)
    try:
        c.execute('''
               INSERT INTO ruangan_rs(koders,koderuangan,tipe,jmlbed,harga)
               VALUES ('{}', '{}', '{}', '{}', '{}' )
               '''.format(rs, str_kode_ruangan, tipe, 0, harga))
        print("huha huha")
    except Exception as e:
        print(e)

    return redirect('/RumahSakit/RuanganRS')

def createBed(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS'):
        if (request.method == 'POST'):
            return createBedUser(request)
        else:
            return showCreateBed(request)
    else:
        # w
        return redirect('/')



def showCreateBed(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    username = request.session.get('username')
    c.execute('''SELECT kode_faskes FROM Rumah_sakit ''')
    rumahSakit = namedtuplefetchall(c)
    c.execute('''SELECT koderuangan FROM ruangan_rs''')
    ruangan = namedtuplefetchall(c)

    context = {
    'rsList': rumahSakit,
    'ruangan':ruangan
    }
    return render(request, 'createBed.html', context)

def createBedUser(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    rs = data['koders']
    ruangan = data['ruangan']
    c.execute('''
               SELECT SUBSTRING (kodebed, 4)AS kode_bed, SUBSTRING(kodebed,1,4) AS str_bed   FROM bed_rs where koders = '{}'
               and koderuangan = '{}'
               order by kodebed desc
               '''.format(rs,ruangan))
    res = namedtuplefetchall(c)
    if res:
        kode_bed = res[0].kode_bed
        kode_bed = int(kode_bed) + 1
        str_bed = res[0].str_bed
    else:
        str_bed = 'BED0'
        kode_bed = 1

    str_kode_bed = str_bed + str(kode_bed)
    try:
        c.execute('''
                   INSERT INTO bed_rs(koders,koderuangan,kodebed)
                   VALUES ('{}', '{}', '{}' )
                   '''.format(rs,ruangan, str_kode_bed))
        print("huha huha")
    except Exception as e:
        print(e)

    return redirect('/RumahSakit/bed')


def createRuanganRSUser(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    rs = data['koders']
    tipe = data['tipe']
    harga = data['harga']
    c.execute('''
           SELECT SUBSTRING (koderuangan, 3)AS kode_ruangan, SUBSTRING(koderuangan,1,3) AS str_ruangan   FROM ruangan_rs where koders = '{}'
           order by koderuangan desc
           '''.format(rs))
    res = namedtuplefetchall(c)
    if res:
        kode_ruangan = res[0].kode_ruangan
        kode_ruangan = int(kode_ruangan)+1
        str_ruangan = res[0].str_ruangan
    else:
        str_ruangan = 'RS00'
        kode_ruangan = 1

    str_kode_ruangan = str_ruangan + str(kode_ruangan)
    try:
        c.execute('''
               INSERT INTO ruangan_rs(koders,koderuangan,tipe,jmlbed,harga)
               VALUES ('{}', '{}', '{}', '{}', '{}' )
               '''.format(rs, str_kode_ruangan, tipe, 0, harga))
        print("huha huha")
    except Exception as e:
        print(e)

    return redirect('/RumahSakit/RuanganRS')


def RuanganRS(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS'):
        if (request.method == 'POST'):
            return ruanganRSUser(request)
        else:
            return showRuanganRS(request)
    else:
        # w
        return redirect('/')

def showRuanganRS(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    c.execute('''
       SELECT * FROM Ruangan_rs ORDER BY ruangan_rs.koders asc
       ''')
    ruangan = namedtuplefetchall(c)
    # print(ruangan)
    context = {
        'ruanganrs': ruangan,
    }
    return render(request, 'ruanganRS.html', context)

def ruanganRSUser(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    rs = data['rs']
    ruangan = data['kodeRuangan']
    tipe = data['tipe']
    harga = data['harga']


    c.execute('''
                          UPDATE ruangan_rs
                          set tipe = '{}' ,
                          harga = '{}'
                          WHERE koders = '{}' 
                          AND koderuangan = '{}'
                          '''.format(tipe,harga,rs, ruangan))
    return redirect('dokter:RuanganRS')



def bed(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    role = request.session.get('role')
    if (role == 'SATGAS'):
        if (request.method == 'POST'):
            return bedDelete(request)
        else:
            return showbed(request)
    else:
        # w
        return redirect('/')

def showbed(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")

    c.execute('''
           SELECT b.koderuangan, b.koders, b.kodebed, r.kodepasien, r.tglkeluar  FROM bed_rs as b 
           LEFT JOIN reservasi_rs as r on
              r.koders = b.koders and r.koderuangan = b.koderuangan and r.kodebed = b.kodebed
           ORDER BY koders asc
           ''')
    bed = namedtuplefetchall(c)
    print(bed)
    today = datetime.date.today()
    context = {
        'bed': bed,
        'today':today,
    }
    return render(request, 'bed.html', context)

def bedDelete(request):
    c = connection.cursor()
    c.execute("set search_path to siruco")
    data = request.POST
    username = request.session.get('username')
    rs = data['rs']
    ruangan = data['ruangan']
    c.execute('''
                     DELETE FROM bed_rs
                     WHERE koderuangan = '{}' 
                     AND koders = '{}'
                     '''.format(ruangan, rs,))
    return redirect('dokter:bed')
