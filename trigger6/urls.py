from django.urls import path
from . import views

app_name = 'trigger6'

urlpatterns = [
    path('transaksi/rs/', views.listTransaksiRS, name = 'listTransaksiRS'),
    path('transaksi/rs/<str:id_transaksi>', views.detailTransaksiRS, name = 'detailTransaksiRS'),
    path('transaksi/rs/update/<str:id_transaksi>', views.updateTransaksiRS, name = 'updateTransaksiRS'),
    
    path('transaksi/hotel/', views.listTransaksiHotel, name = 'listTransaksiHotel'),
    path('transaksi/hotel/<str:id_transaksi>', views.detailTransaksiHotel, name = 'detailTransaksiHotel'),
    path('transaksi/hotel/update/<str:id_transaksi>', views.updateTransaksiHotel, name = 'updateTransaksiHotel'),
    
    path('hasil-tes/', views.listHasilTesPasien, name = 'listHasilTesPasien'),
    path('hasil-tes/create/', views.createHasilTesPasien, name = 'createHasilTesPasien'),
]
