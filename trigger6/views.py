from django.shortcuts import render, redirect
from django.contrib import messages
from trigger6.utils import *

# 19. Membayar Transaksi RS [READ]
def listTransaksiRS(request):
    context = {
        'listTransaksi': getListTransaksiRS(request.session['username'])
    }
    return render(request, 'transaksi_rs.html', context)

# 19. Membayar Transaksi RS [READ]
def detailTransaksiRS(request, id_transaksi):
    context = {
        'data': getDetailTransaksiRS(id_transaksi)
    }
    return render(request, 'detail_transaksi_rs.html', context)

# 19. Membayar Transaksi RS [UPDATE]
def updateTransaksiRS(request, id_transaksi):
    updateStatusTransaksiRS(id_transaksi)
    return redirect('/transaksi/rs')

# 20. Membayar Transaksi Hotel [READ]
def listTransaksiHotel(request):
    context = {
        'listTransaksi': getListTransaksiHotel(request.session['username'])
    }
    return render(request, 'transaksi_hotel.html', context)

# 20. Membayar Transaksi Hotel [READ]
def detailTransaksiHotel(request, id_transaksi):
    context = {
        'data': getDetailTransaksiHotel(id_transaksi),
        'booking': getDetailTransaksiBooking(id_transaksi),
        'listMakan': getDetailTransaksiMakan(id_transaksi)
    }
    return render(request, 'detail_transaksi_hotel.html', context)

# 20. Membayar Transaksi Hotel [UPDATE]
def updateTransaksiHotel(request, id_transaksi):
    updateStatusTransaksiHotel(id_transaksi)
    return redirect('/transaksi/hotel')

# 21. Hasil Tes Pasien [READ]
def listHasilTesPasien(request):
    context = {
        'listHasil': getListHasilTesPasien(request.session['username'], request.session['role'])
    }
    return render(request, 'hasil_tes.html', context)

# 21. Hasil Tes Pasien [CREATE]
def createHasilTesPasien(request):
    if (request.method == "POST"):
        return addHasilTesPasien(request)
    else:
        return formHasilTesPasien(request)

# 21. Hasil Tes Pasien [CREATE] [GET]
def formHasilTesPasien(request):
    context = {
        'listPasien': getListPasien()
    }
    return render(request, 'form_hasil_tes.html', context)

# 21. Hasil Tes Pasien [CREATE] [POST]
def addHasilTesPasien(request):
    data = request.POST
    nik = data['pasien']
    tanggal = data['tanggal']
    tipe = data['tipe']
    status = data['status']
    nilaict = '-' if data['nilaict'] == '' else data['nilaict']
    
    if (tanggal == '') or (nilaict == '-' and tipe == 'PCR'):
        messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu.')
        return redirect('trigger6:createHasilTesPasien')
    else:
        createHasilTesPasienOlehDokter(nik, tanggal, tipe, status, nilaict)
        return redirect('trigger6:listHasilTesPasien')