from django.db import connection
from django.http import Http404

from login.views import dictfetchall, dictfetchone

# 19. Membayar Transaksi RS [READ]
def getListTransaksiRS(username):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM TRANSAKSI_RS as TRS, PASIEN as P
            WHERE
                TRS.kodepasien = P.nik AND
                P.idpendaftar = %s;
            """,
            [username],
        )
        return dictfetchall(cursor)

# 19. Membayar Transaksi RS [READ]
def getDetailTransaksiRS(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM
                TRANSAKSI_RS as TRS
                    NATURAL JOIN RESERVASI_RS as RRS,
                FASKES as F
            WHERE
                TRS.idtransaksi = %s AND
                RRS.koders = F.kode;
            """,
            [idtransaksi],
        )
        return dictfetchone(cursor)


# 19. Membayar Transaksi RS [UPDATE]
def updateStatusTransaksiRS(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            UPDATE TRANSAKSI_RS
            SET 
                statusbayar = 'Lunas', 
                tglpembayaran = CURRENT_DATE, 
                waktupembayaran = CURRENT_TIMESTAMP
            WHERE idtransaksi = %s;
            """,
            [idtransaksi],
        )

# 20. Membayar Transaksi Hotel [READ]
def getListTransaksiHotel(username):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM TRANSAKSI_HOTEL as TH, PASIEN as P
            WHERE
                TH.kodepasien = P.nik AND
                P.idpendaftar = %s;
            """,
            [username],
        )
        return dictfetchall(cursor)

# 20. Membayar Transaksi Hotel [READ]
def getDetailTransaksiHotel(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM
                TRANSAKSI_HOTEL as TH
                    NATURAL JOIN RESERVASI_HOTEL as RH,
                HOTEL as H
            WHERE
                TH.idtransaksi = %s AND
                RH.kodehotel = H.kode;
            """,
            [idtransaksi],
        )
        return dictfetchone(cursor)

# 20. Membayar Transaksi Hotel [READ]
def getDetailTransaksiBooking(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT totalbayar
            FROM TRANSAKSI_BOOKING
            WHERE idtransaksibooking = %s;
            """,
            [idtransaksi],
        )
        return dictfetchone(cursor)

# 20. Membayar Transaksi Hotel [READ]
def getDetailTransaksiMakan(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT totalbayar
            FROM TRANSAKSI_MAKAN
            WHERE idtransaksi = %s;
            """,
            [idtransaksi],
        )
        return dictfetchall(cursor)

# 20. Membayar Transaksi Hotel [UPDATE]
def updateStatusTransaksiHotel(idtransaksi):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            UPDATE TRANSAKSI_HOTEL
            SET 
                statusbayar = 'Lunas', 
                tanggalpembayaran = CURRENT_DATE, 
                waktupembayaran = CURRENT_TIMESTAMP
            WHERE idtransaksi = %s;
            """,
            [idtransaksi],
        )

# 21. Hasil Tes Pasien [READ] [GET]
def getListHasilTesPasien(username, role):
    with connection.cursor() as cursor:
        if role == 'PENGGUNA_PUBLIK':
            cursor.execute(
                """
                SELECT *
                FROM TES as T, PASIEN as P
                WHERE
                    T.nik_pasien = P.nik AND
                    P.idpendaftar = %s;
                """,
                [username],
            )
        else:
            cursor.execute(
                """
                SELECT *
                FROM TES
                ORDER BY tanggaltes ASC;
                """
            )
        return dictfetchall(cursor)

# 21. Hasil Tes Pasien [READ] [POST]
def getListPasien():
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT nik
            FROM PASIEN
            ORDER BY nik ASC;
            """
        )
        return dictfetchall(cursor)

# 21. Hasil Tes Pasien [CREATE]
def createHasilTesPasienOlehDokter(nik, tanggal, tipe, status, nilaict):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            INSERT INTO TES(nik_pasien, tanggaltes, jenis, status, nilaict)
            VALUES ('{}', '{}', '{}', '{}', '{}')
            """.format(nik, tanggal, tipe, status, nilaict)
        )
    