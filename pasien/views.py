from django.shortcuts import render, redirect
from django.contrib import messages
from pasien.utils import *

# 03. Pasien [CREATE]
def daftarPasien(request):
    if (request.method == "POST"):
        return createPasien(request)
    else:
        return formDaftar(request)

# 03. Pasien [CREATE] [GET]
def formDaftar(request):
    return render(request, 'form_pasien.html', {'username': request.session['username']})

# 03. Pasien [CREATE] [POST]
def createPasien(request):
    data = request.POST

    nik = data['nik']
    idpendaftar = request.session['username']
    nama = data['nama']
    ktp_jalan = data['ktp_jalan']
    ktp_kelurahan = data['ktp_kelurahan']
    ktp_kecamatan = data['ktp_kecamatan']
    ktp_kabkot = data['ktp_kabkot']
    ktp_prov = data['ktp_prov']
    dom_jalan = data['dom_jalan']
    dom_kelurahan = data['dom_kelurahan']
    dom_kecamatan = data['dom_kecamatan']
    dom_kabkot = data['dom_kabkot']
    dom_prov = data['dom_prov']
    notelp = data['notelp']
    nohp  = data['nohp']

    if nik == '' or nama == '' or ktp_jalan == '' or ktp_kelurahan == '' or \
        ktp_kecamatan == '' or ktp_kabkot == '' or ktp_prov == '' or dom_jalan == '' or \
        dom_kelurahan == '' or dom_kecamatan == '' or dom_kabkot == '' or dom_prov == '' or \
        notelp == '' or nohp == '':
        messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu.')
        return redirect('pasien:daftarPasien')
    else:
        createDataPasien(nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, 
                    ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
                    dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp)
        return redirect('pasien:listPasien')

# 03. Pasien [READ]
def listPasien(request):
    context = {
        'listPasien': getListPasien(request.session['username'])
    }
    return render(request, 'pasien.html', context)

# 03. Pasien [READ]
def detailPasien(request, nik_pasien):
    context = {
        'data': getDetailPasien(nik_pasien)
    }
    return render(request, 'detail_pasien.html', context)

# 03. Pasien [UPDATE]
def updatePasien(request, nik_pasien):
    if (request.method == "POST"):
        return postUpdatePasien(request, nik_pasien)
    else:
        return formUpdatePasien(request, nik_pasien)

# 03. Pasien [UPDATE] [GET]
def formUpdatePasien(request, nik_pasien):
    return render(request, 'form_update_pasien.html', {'data': getDetailPasien(nik_pasien)})

# 03. Pasien [UPDATE] [POST]
def postUpdatePasien(request, nik_pasien):
    data = request.POST

    nik = data['nik']
    idpendaftar = data['idpendaftar']
    nama = data['nama']
    ktp_jalan = data['ktp_jalan']
    ktp_kelurahan = data['ktp_kelurahan']
    ktp_kecamatan = data['ktp_kecamatan']
    ktp_kabkot = data['ktp_kabkot']
    ktp_prov = data['ktp_prov']
    dom_jalan = data['dom_jalan']
    dom_kelurahan = data['dom_kelurahan']
    dom_kecamatan = data['dom_kecamatan']
    dom_kabkot = data['dom_kabkot']
    dom_prov = data['dom_prov']
    notelp = data['notelp']
    nohp  = data['nohp']

    if nik == '' or nama == '' or ktp_jalan == '' or ktp_kelurahan == '' or \
        ktp_kecamatan == '' or ktp_kabkot == '' or ktp_prov == '' or dom_jalan == '' or \
        dom_kelurahan == '' or dom_kecamatan == '' or dom_kabkot == '' or dom_prov == '' or \
        notelp == '' or nohp == '':
        messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu.')
        return redirect('pasien:updatePasien', nik_pasien=nik_pasien)
    else:
        updateDataPasien(nik, ktp_jalan, ktp_kelurahan, ktp_kecamatan, 
                    ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
                    dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp)
        return redirect('pasien:listPasien')

# 03. Pasien [DELETE]
def deletePasien(request, nik_pasien):
    deleteDataPasien(nik_pasien)
    return redirect('pasien:listPasien')

