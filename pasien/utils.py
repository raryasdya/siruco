from django.db import connection
from django.http import Http404

from login.views import dictfetchall, dictfetchone

# 03. Pasien [CREATE]
def createDataPasien(nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, 
                ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
                dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            INSERT INTO PASIEN(
                nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, 
                ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan,
                dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp
                )
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
            """,
            [
                nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, 
                ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
                dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp
            ],
        )

# 03. Pasien [READ]
def getListPasien(username):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM PASIEN
            WHERE idpendaftar = %s;
            """,
            [username],
        )
        return dictfetchall(cursor)

# 03. Pasien [READ]
def getDetailPasien(nik_pasien):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT *
            FROM PASIEN
            WHERE nik = %s;
            """,
            [nik_pasien],
        )
        return dictfetchone(cursor)

# 03. Pasien [UPDATE]
def updateDataPasien(nik, ktp_jalan, ktp_kelurahan, 
                ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
                dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp):
    print(nik, ktp_jalan, ktp_kelurahan, 
            ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, 
            dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp, sep="\n")
    with connection.cursor() as cursor:
        cursor.execute(
            """
            UPDATE PASIEN
            SET
                ktp_jalan=%s, ktp_kelurahan=%s, ktp_kecamatan=%s, ktp_kabkot=%s, 
                ktp_prov=%s, dom_jalan=%s, dom_kelurahan=%s, dom_kecamatan=%s, 
                dom_kabkot=%s, dom_prov=%s, notelp=%s, nohp=%s
            WHERE nik = %s
            """,
            [
                ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, 
                ktp_prov, dom_jalan, dom_kelurahan, dom_kecamatan, 
                dom_kabkot, dom_prov, notelp, nohp, nik
            ],
        )

# 03. Pasien [DELETE]
def deleteDataPasien(nik_pasien):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            DELETE FROM PASIEN 
            WHERE nik = %s;
            """,
            [nik_pasien],
        )