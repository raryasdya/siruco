from django.urls import path
from . import views

app_name = 'pasien'

urlpatterns = [
    path('', views.listPasien, name = 'listPasien'),
    path('daftar/', views.daftarPasien, name = 'daftarPasien'),
    path('detail/<str:nik_pasien>', views.detailPasien, name = 'detailPasien'),
    path('delete/<str:nik_pasien>', views.deletePasien, name = 'deletePasien'),
    path('update/<str:nik_pasien>', views.updatePasien, name = 'updatePasien'),
]
