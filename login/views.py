from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages

#GENERAL FUNCTION
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dictfetchone(cursor):
    "Return one row from a cursor as a dict"
    data = cursor.fetchone()
    if data is None:
        return None

    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, data))

def dictfetchall(cursor):
    "Return all rows from a cursor as a list of dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

# login
def login(request):
    if (request.method == 'POST'):
        return loginUser(request)
    else:
        return showLogin(request)


# login page
def showLogin(request, validasi = None):
        message=""
        if(validasi == False):
            message = "Invalid email or password"
        context ={
            'message':message
        }
        return render(request, 'login.html', context)


# login logic
def loginUser(request):
    username = request.POST['username']
    password = request.POST['password']
    role = None
    try:
        with connection.cursor() as c:
            c.execute(''' 
                    SELECT * FROM akun_pengguna 
                    WHERE username='{}' AND password='{}'
                '''.format(username, password))
            data_user = dictfetchone(c)
            if (data_user != None):
                request.session['username'] = username
                request.session['role'] = data_user['peran']
                return redirect('/')
            else:
                return showLogin(request,False)
    except:
        return render(request, 'login.html')


# register
def register(request):
    if (request.method == "POST"):
        return registerUser(request)
    else:
        return showRegister(request)


# register page
def showRegister(request, validation = None):
    c =connection.cursor()
    c.execute("set search_path to siruco")
    c.execute('''
            SELECT *  FROM faskes
        ''')
    message = ""
    if(validation == False):
        message = "Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka"
    faskes = namedtuplefetchall(c)
    context={
        'message':message,
        'faskes_list':faskes,
    }
    return render(request, 'register.html',context)


# register logic
def registerUser(request):
    data = request.POST
    tipe = data['tipe']
    username = data['email']
    password = data['password']
    fullname = data['fullname']
    str = data['str']
    nik = data['nik']
    hp = data['hp']
    gelardepan = data['gelar-depan']
    gelarbelakang = data['gelar-belakang']
    faskes = data['faskes']
    status = 'AKTIF'
    peran = 'Penanggung Jawab'

    try:
        with connection.cursor() as c:
            c.execute("set search_path to siruco")
            if(tipe=='satgas'):
                c.execute('''
                    INSERT INTO AKUN_PENGGUNA(username,password,peran)
                    VALUES ('{}', '{}', '{}')
                    '''.format(username, password, 'SATGAS'))
                
                c.execute('''
                    INSERT INTO ADMIN(username)
                    VALUES ('{}')
                    '''.format(username))
                
                c.execute('''
                    INSERT INTO ADMIN_SATGAS(username,idfaskes)
                    VALUES ('{}', '{}')
                    '''.format(username,faskes))
            
            elif (tipe == 'sistem'):
                c.execute('''
                    INSERT INTO AKUN_PENGGUNA(username,password, peran)
                    VALUES ('{}', '{}', '{}')
                    '''.format(username, password, 'ADMIN_SISTEM'))

                c.execute('''
                    INSERT INTO ADMIN(username)
                    VALUES ('{}')
                    '''.format(username))

            elif (tipe == 'dokter'):
                c.execute('''
                    INSERT INTO AKUN_PENGGUNA(username,password, peran)
                    VALUES ('{}', '{}', '{}')
                    '''.format(username, password, 'DOKTER'))

                c.execute('''
                    INSERT INTO ADMIN(username)
                    VALUES ('{}')
                    '''.format(username))

                c.execute('''
                    INSERT INTO DOKTER(username,nostr,nama,nohp,gelardepan,gelarbelakang)
                    VALUES ('{}', '{}', '{}', '{}', '{}', '{}')
                    '''.format(username, str,fullname,hp,gelardepan,gelarbelakang))

            elif (tipe == 'pengguna'):
                c.execute('''
                    INSERT INTO AKUN_PENGGUNA(username,password, peran)
                    VALUES ('{}', '{}', '{}')
                    '''.format(username, password, 'PENGGUNA_PUBLIK'))

                c.execute('''
                    INSERT INTO PENGGUNA_PUBLIK(username,nik,nama,status,peran,nohp)
                    VALUES ('{}', '{}', '{}', '{}', '{}', '{}')
                    '''.format(username, nik, fullname,status,peran,hp))

    except Exception as e:
        print(e)
        return showRegister(request,False)

    return redirect('/login')


# logout -> redirect to login page
def logout(request):
    request.session.flush()
    return redirect('/login')
